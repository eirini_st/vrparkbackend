﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VrPark.Database;
using VrPark.Database.Models;

namespace VrPark.Services
{
    public class ParkItemService : ServiceBase<ParkItem>
    {
        public ParkItemService(ApplicationDbContext db) : base(db)
        {
        }
    }
}
