﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using VrPark.Database;
using System.Data;

namespace VrPark.Services
{
    public abstract class ServiceBase<T> : IService<T> where T : class
    {
        protected ApplicationDbContext db;
        protected IQueryable<T> query;
        private bool disposed = false;

        public ServiceBase(ApplicationDbContext db)
        {
            db.Configuration.ProxyCreationEnabled = false;
            this.db = db;
            query = db.Set<T>();
        }

        /// <summary>
        /// Find entry by id (primary key)
        /// </summary>
        /// <param name="id">Entry's Id</param>
        /// <returns>Single or null entry of type T</returns>
        public T FindById(int id)
        {
            return db.Set<T>().Find(id);
        }

        /// <summary>
        /// Get all entries
        /// </summary>
        /// <returns>Queryiable of type T</returns>
        public virtual IQueryable<T> GetAll()
        {
            return query;
        }

        public virtual IQueryable<T> GetAll(int pageRecords, int offset, string order)
        {
            return query.OrderBy(order).Skip(offset).Take(pageRecords);
        }


        /// <summary>
        /// Find by given expression
        /// </summary>
        /// <param name="condition">expression (eg: a => e.Name == "test")</param>
        /// <returns>Queryiable of type T</returns>
        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> condition)
        {
            return query.Where(condition);
        }

        /// <summary>
        /// Adds entry to DdSet
        /// </summary>
        /// <param name="entity">Entry to add</param>
        public virtual void Add(T entity)
        {
            db.Set<T>().Add(entity);
        }

        /// <summary>
        /// Adds a collection of entries to DdSet
        /// </summary>
        /// <param name="entities">Entries to add</param>
        public virtual void AddRange(IEnumerable<T> entities)
        {
            db.Set<T>().AddRange(entities);
        }

        /// <summary>
        /// Delete a detached entry from DbSet
        /// </summary>
        /// <param name="entity">Entry to delete</param>
        public virtual void AttachDelete(T entity)
        {
            db.Set<T>().Attach(entity);
            db.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Delete an entry from DbSet
        /// </summary>
        /// <param name="entity">Entry to delete</param>
        public virtual void Delete(T entity)
        {
            db.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Edit a detached entry from DbSet
        /// </summary>
        /// <param name="entity">Edited entry</param>
        public virtual void AttachEdit(T entity)
        {
            db.Set<T>().Attach(entity);
            Edit(entity);
        }

        /// <summary>
        /// Edit an entry from DbSet
        /// </summary>
        /// <param name="entity">Edited entry</param>
        public virtual void Edit(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Edit and Save an entry from DbSet
        /// </summary>
        /// <param name="entity">Edited entry</param>
        public virtual void EditAndSave(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            Save();
        }

        /// <summary>
        /// Save changed to database
        /// </summary>
        public virtual void Save()
        {
            db.SaveChanges();
        }

        /// <summary>
        /// Detaches given entity
        /// </summary>
        /// <param name="entity">Detached entity</param>
        public virtual void Detach(T entity)
        {
            db.Entry(entity).State = EntityState.Detached;
        }

        /// <summary>
        /// Detaches a list of entitites
        /// </summary>
        /// <param name="entity">Detached entities</param>
        public virtual void Detach(ICollection<T> entities)
        {
            if (entities != null)
            {
                foreach (T entity in entities)
                {
                    db.Entry(entity).State = EntityState.Detached;
                }
            }
        }

        /// <summary>
        /// Finds by using given dynamic linq expressions
        /// </summary>
        /// <param name="conditions">Dynamic linq expressions</param>
        public List<T> Search(List<string> conditions)
        {
            foreach (string condition in conditions)
            {
                query = query.Where(condition);
            }

            return query.ToList();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }

            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
