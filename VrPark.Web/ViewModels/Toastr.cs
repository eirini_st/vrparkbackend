﻿using System;

namespace VrPark.Web.ViewModels
{
    public class Toastr
    {
        public String Title { get; set; }
        public String Message { get; set; }
        public String Severity { get; set; }
    }
}