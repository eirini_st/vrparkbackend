﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VrPark.Web.ViewModels
{
    public class ParkItemMediaViewModel
    {
        public int Id { get; set; }

        public ParkItemViewModel ParkItem { get; set; }

        public String Name { get; set; }

        public String Type { get; set; }

        public byte[] MediaContent { get; set; }
    }
}
