﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VrPark.Web.ViewModels
{
    public class ParkItemViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "ERR_Required")]
        [Display(Name = "ParkItemCategory", ResourceType = typeof(Resources.Resources))]
        public int ParkItemCategoryId { get; set; }

        public ParkItemCategoryViewModel ParkItemCategory { get; set; }

        public String Code { get; set; }

        public String Name { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public String Description { get; set; }

        public String AdditionalInformation { get; set; }

        public virtual ICollection<ParkItemMediaViewModel> ParkItemMedia { get; set; }
    }
}
