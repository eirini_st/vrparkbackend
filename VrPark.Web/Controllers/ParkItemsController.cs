﻿using AutoMapper;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VrPark.Database.Models;
using System.Net;
using NLog;
using VrPark.Web.ViewModels;
using VrPark.Services;
using VrPark.Web.Helpers;

namespace VrPark.Web.Controllers
{
    public class ParkItemsController : BaseController
    {
        [Inject]
        public ParkItemService ParkItemService { private get; set; }

        [Inject]
        public ParkItemCategoryService ParkItemCategoryService { private get; set; }

        private MapHelper<ParkItem, ParkItemViewModel> mapper;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            mapper = new MapHelper<ParkItem, ParkItemViewModel>();
        }

        // GET: ParkItems
        public ActionResult Index()
        {
            var parkItems = ParkItemService.GetAll().ToList();
            return View(mapper.Map(parkItems));
        }

        // GET: ParkItems/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.Create = "true";
            ViewBag.ParkItemCategoryId = new SelectList(ParkItemCategoryService.GetAll(), "Id", "Code");
            return View();
        }

        // POST: ParkItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ParkItemViewModel parkItemViewModel)
        {
            var parkItem = Mapper.Map<ParkItemViewModel, ParkItem>(parkItemViewModel);

            if (ModelState.IsValid)
            {
                try
                {
                    ParkItemService.Add(parkItem);
                    ParkItemService.Save();

                    // sweet alert for successful save

                    TempData["Toastr"] = new Toastr
                    {
                        Title = Resources.Resources.ParkItems,
                        Message = Resources.Resources.EntrySaveSuccess,
                        Severity = "success"
                    };

                    logger.Info("New park item created with id: {0}", parkItem.Id);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    // sweet alert for error save
                    logger.Error(ex, "Error in saving a new park item in db");
                    TempData["Toastr"] = new Toastr
                    {
                        Title = Resources.Resources.ParkItems,
                        Message = Resources.Resources.EntrySaveError,
                        Severity = "error"
                    };
                }
            }

            return View(parkItemViewModel);
        }

        // GET: ParkItems/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParkItem parkItem = ParkItemService.FindById(id.Value);

            if (parkItem == null)
            {
                return HttpNotFound();
            }

            return View(Mapper.Map<ParkItem, ParkItemViewModel>(parkItem));
        }

        // POST: ParkItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ParkItemViewModel parkItemViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var parkItem = ParkItemService.FindBy(p => p.Id == parkItemViewModel.Id).Single();
                    parkItem = Mapper.Map<ParkItemViewModel, ParkItem>(parkItemViewModel);

                    var parkItemCategory = ParkItemCategoryService.FindBy(pc => pc.Id == parkItem.ParkItemCategoryId).FirstOrDefault();
                    parkItem.ParkItemCategory = parkItemCategory;

                    ParkItemService.EditAndSave(parkItem);
                    TempData["Toastr"] = new Toastr
                    {
                        Title = Resources.Resources.ParkItems,
                        Message = Resources.Resources.EntrySaveSuccess,
                        Severity = "success"
                    };

                    logger.Info("Park item with id: {0} edited successfully", parkItem.Id);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    // Error of unique account name
                    if (ex.InnerException != null && ex.InnerException.InnerException != null && ((System.Data.SqlClient.SqlException)ex.InnerException.InnerException).Number == 2601)
                    {
                        TempData["Toastr"] = new Toastr
                        {
                            Title = Resources.Resources.ParkItems,
                            Message = Resources.Resources.ParkItemDuplicateName,
                            Severity = "error"
                        };
                    }
                    else
                    {
                        TempData["Toastr"] = new Toastr
                        {
                            Title = Resources.Resources.ParkItems,
                            Message = Resources.Resources.EntrySaveError,
                            Severity = "error"
                        };
                    }

                    logger.Error(ex, "Error in editing an existing park with id: {0}", parkItemViewModel.Id);
                }
            }

            return View(parkItemViewModel);
        }

        // POST: ParkItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var parkItem = ParkItemService.FindBy(pit => pit.Id == id).Single();

            if (parkItem == null)
            {
                return HttpNotFound();
            }

            try
            {
                ParkItemService.Delete(parkItem);
                ParkItemService.Save();

                TempData["Toastr"] = new Toastr
                {
                    Title = Resources.Resources.ParkItems,
                    Message = Resources.Resources.EntryDeleteSuccess,
                    Severity = "success"
                };

                logger.Info("Successful deleting of an existing park with id: {0}", parkItem.Id);
            }
            catch (Exception ex)
            {
                TempData["Toastr"] = new Toastr
                {
                    Title = Resources.Resources.ParkItems,
                    Message = Resources.Resources.EntryDeleteError,
                    Severity = "error"
                };

                logger.Error(ex, "Error in deleting an existing park with id: {0}", parkItem.Id);
            }

            return RedirectToAction("Index");
        }
    }
}