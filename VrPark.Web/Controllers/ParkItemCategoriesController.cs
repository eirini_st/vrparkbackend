﻿using Ninject;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VrPark.Database;
using VrPark.Database.Models;
using VrPark.Services;
using VrPark.Web.Helpers;
using VrPark.Web.ViewModels;

namespace VrPark.Web.Controllers
{
    public class ParkItemCategoriesController : BaseController
    {
        [Inject]
        public ParkItemCategoryService ParkItemCategoryService { private get; set; }

        private MapHelper<ParkItemCategory, ParkItemCategoryViewModel> mapper;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            this.mapper = new MapHelper<ParkItemCategory, ParkItemCategoryViewModel>();
        }

        // GET: ParkItemCategories
        public ActionResult Index()
        {
            var parkItemCategories = ParkItemCategoryService.GetAll().ToList();
            return View(mapper.Map(parkItemCategories));
        }

        // GET: ParkItemCategories/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.Create = "true";
            return View();
        }

        // POST: ParkItemCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ParkItemCategoryViewModel parkItemCategoryViewModel)
        {
            var parkItemCategory = mapper.Map(parkItemCategoryViewModel);

            if (ModelState.IsValid)
            {
                try
                {
                    ParkItemCategoryService.Add(parkItemCategory);
                    ParkItemCategoryService.Save();

                    // sweet alert for successful save

                    TempData["Toastr"] = new Toastr
                    {
                        Title = Resources.Resources.ParkItems,
                        Message = Resources.Resources.EntrySaveSuccess,
                        Severity = "success"
                    };

                    logger.Info("New park item created with id: {0}", parkItemCategory.Id);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    // sweet alert for error save
                    logger.Error(ex, "Error in saving a new park item in db");
                    TempData["Toastr"] = new Toastr
                    {
                        Title = Resources.Resources.ParkItemCategories,
                        Message = Resources.Resources.EntrySaveError,
                        Severity = "error"
                    };
                }
            }

            return View(parkItemCategoryViewModel);
        }

        // GET: ParkItemCategories/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ParkItemCategory parkItemCategory = ParkItemCategoryService.FindById(id.Value);

            if (parkItemCategory == null)
            {
                return HttpNotFound();
            }

            return View(mapper.Map(parkItemCategory));
        }


        // POST: ParkItemCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ParkItemCategoryViewModel parkItemCategoryViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var parkItemCategory = ParkItemCategoryService.FindBy(p => p.Id == parkItemCategoryViewModel.Id).Single();
                    parkItemCategory = mapper.Map(parkItemCategoryViewModel);

                    ParkItemCategoryService.EditAndSave(parkItemCategory);
                    TempData["Toastr"] = new Toastr
                    {
                        Title = Resources.Resources.ParkItemCategories,
                        Message = Resources.Resources.EntrySaveSuccess,
                        Severity = "success"
                    };

                    logger.Info("Park item with id: {0} edited successfully", parkItemCategory.Id);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    // Error of unique account name
                    if (ex.InnerException != null && ex.InnerException.InnerException != null && ((System.Data.SqlClient.SqlException)ex.InnerException.InnerException).Number == 2601)
                    {
                        TempData["Toastr"] = new Toastr
                        {
                            Title = Resources.Resources.ParkItems,
                            Message = Resources.Resources.ParkItemDuplicateName,
                            Severity = "error"
                        };
                    }
                    else
                    {
                        TempData["Toastr"] = new Toastr
                        {
                            Title = Resources.Resources.ParkItems,
                            Message = Resources.Resources.EntrySaveError,
                            Severity = "error"
                        };
                    }

                    logger.Error(ex, "Error in editing an existing park with id: {0}", parkItemCategoryViewModel.Id);
                }
            }

            return View(parkItemCategoryViewModel);
        }

        // POST: ParkItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var parkItemCategory = ParkItemCategoryService.FindBy(pit => pit.Id == id).Single();

            if (parkItemCategory == null)
            {
                return HttpNotFound();
            }

            try
            {
                ParkItemCategoryService.Delete(parkItemCategory);
                ParkItemCategoryService.Save();

                TempData["Toastr"] = new Toastr
                {
                    Title = Resources.Resources.ParkItems,
                    Message = Resources.Resources.EntryDeleteSuccess,
                    Severity = "success"
                };

                logger.Info("Successful deleting of an existing park with id: {0}", parkItemCategory.Id);
            }
            catch (Exception ex)
            {
                TempData["Toastr"] = new Toastr
                {
                    Title = Resources.Resources.ParkItems,
                    Message = Resources.Resources.EntryDeleteError,
                    Severity = "error"
                };

                logger.Error(ex, "Error in deleting an existing park with id: {0}", parkItemCategory.Id);
            }

            return RedirectToAction("Index");
        }
    }
}

