﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VrPark.Web.Startup))]
namespace VrPark.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
