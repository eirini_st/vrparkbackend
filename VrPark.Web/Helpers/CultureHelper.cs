﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace VrPark.Web.Helpers
{
    public static class CultureHelper
    {

        private static readonly HashSet<string> _cultures = new HashSet<string> { "el-GR", "en-US" };
        private static readonly string _defaultCulture = _cultures.ElementAt(0);
        private static readonly HashSet<string> _neutralCultures = new HashSet<string>(_cultures.Select(c => c.Contains('-') ? c.Split('-')[0] : c));

        public static string GetDefaultCulture()
        {
            return _defaultCulture;
        }

        public static string GetCulture(string culture)
        {
            return GetCulture(new List<string> { culture });
        }

        public static string GetCulture(List<string> cultures)
        {
            List<string> neutralCultures = _cultures.Select(c => c.Contains('-') ? c.Split('-')[0] : c).ToList();

            string result = cultures.Where(c => _cultures.Select(_c => _c.ToLowerInvariant()).Contains(c.ToLowerInvariant())).FirstOrDefault();

            if (result == null)
            {
                result = neutralCultures.Where(c => _cultures.Select(_c => _c.ToLowerInvariant()).Contains(c.ToLowerInvariant())).FirstOrDefault();
            }

            if (result == null)
            {
                result = neutralCultures.Where(c => _neutralCultures.Select(_c => _c.ToLowerInvariant()).Contains(c.ToLowerInvariant())).FirstOrDefault();
            }

            return result;
        }

        public static string GetCurrentCulrture(bool fullName = false)
        {
            String culture;

            if (fullName)
            {
                culture = Resources.Resources.ResourceManager.GetString(Thread.CurrentThread.CurrentUICulture.ToString().Replace("-", "_"));
            }
            else
            {
                culture = Thread.CurrentThread.CurrentUICulture.ToString();
            }

            return culture;
        }
    }
}