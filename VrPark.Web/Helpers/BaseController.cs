﻿using System;
using System.Threading;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;

namespace VrPark.Web.Helpers
{
    public class BaseController : Controller
    {
        protected static Logger logger = NLog.LogManager.GetLogger("Controller");
        protected string _controllerAtActionMsg;

        public BaseController() { }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string culture = null;

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (culture == null && cultureCookie != null && !string.IsNullOrWhiteSpace(cultureCookie.Value))
            {
                culture = CultureHelper.GetCulture(cultureCookie.Value);
            }

            if (culture == null && Request.UserLanguages != null && Request.UserLanguages.Length > 0)
            {
                culture = CultureHelper.GetCulture(Request.UserLanguages.ToList());
            }

            if (culture == null)
            {
                culture = CultureHelper.GetDefaultCulture();
            }

            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(culture);
            //Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUserEmail = "";
            }

            string _actionName = ControllerContext.RouteData.Values["action"].ToString();
            string _controllerName = ControllerContext.RouteData.Values["controller"].ToString();
            _controllerAtActionMsg = _controllerName + "@" + _actionName + ": ";

            return base.BeginExecuteCore(callback, state);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult Culture(string culture, string returnUrl)
        {
            culture = CultureHelper.GetCulture(culture);

            if (culture == null)
            {
                culture = CultureHelper.GetDefaultCulture();
            }

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie == null)
            {
                cultureCookie = new HttpCookie("_culture");
            }
            cultureCookie.Value = culture;
            cultureCookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(cultureCookie);

            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(Request.QueryString["returnUrl"]);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}