﻿using System;
using System.Collections.Generic;
using AutoMapper;
using VrPark.Database.Models;
using VrPark.Web.ViewModels;
using WattVolt.Helpers;

namespace VrPark.Web.Helpers
{
    public class MapHelper<TModel, TViewModel>
    {
        protected MapperConfiguration mapperConfig;
        protected IMapper mapper;

        public MapHelper()
        {
            mapperConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<TModel, TViewModel>();
                cfg.CreateMap<TViewModel, TModel>();
            });

            mapper = mapperConfig.CreateMapper();
        }

        /// <summary>
        /// Maps given Model object to ViewModel
        /// </summary>
        public TViewModel Map(TModel model)
        {
            return mapper.Map<TModel, TViewModel>(model);
        }

        /// <summary>
        /// Maps given ViewModel object to Model
        /// </summary>
        public TModel Map(TViewModel viewmodel)
        {
            return mapper.Map<TViewModel, TModel>(viewmodel);
        }

        /// <summary>
        /// Maps given ViewModel object to given Model object
        /// </summary>
        public void Map(TViewModel viewmodel, TModel model)
        {
            mapper.Map(viewmodel, model);
        }

        /// <summary>
        /// Maps given list of Models object to a list of ViewModel
        /// </summary>
        public IList<TViewModel> Map(IList<TModel> model)
        {
            IList<TViewModel> mapped = mapper.Map<IList<TModel>, IList<TViewModel>>(model);

            return mapped;
        }

        /// <summary>
        /// Maps given list of ViewModel objects to a list of Model
        /// </summary>
        public IList<TModel> Map(IList<TViewModel> model)
        {
            IList<TModel> mapped = mapper.Map<IList<TViewModel>, IList<TModel>>(model);

            return mapped;
        }

        /// <summary>
        /// Maps given ViewModel object to given Model object
        /// </summary>
        public void Map(IList<TModel> model, IList<TViewModel> viewmodel)
        {
            mapper.Map(model, viewmodel);
        }

    }
}