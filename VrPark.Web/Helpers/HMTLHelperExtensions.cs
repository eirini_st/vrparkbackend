﻿using System;
using System.Web.Mvc;

namespace VrPark.Web.Helpers
{
    public static class HMTLHelperExtensions
    {
        public static string IsSelected(this HtmlHelper html, string controller = null, string action = null, bool isDropdown = true)
        {
            string cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            if (controller != currentController || action != currentAction) {
                if (isDropdown)
                {
                    cssClass = "dropdown";
                }
                else
                {
                    cssClass = String.Empty;
                }
            }
                
            return cssClass;
        }

        public static string IsSelectedController(this HtmlHelper html, string[] controllers = null, bool isDropdown = true)
        {
            string cssClass = "active";
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];
            bool isSelected = false;

            if (controllers == null)
                controllers = new string[] { currentController };

            foreach (string controller in controllers)
            {
                if (controller == currentController)
                {
                    isSelected = true;
                }
            }

            if (!isSelected)
            {
                if (isDropdown)
                {
                    cssClass = "dropdown";
                }
                else
                {
                    cssClass = String.Empty;
                }
            }

            return cssClass;
        }
	}
}
