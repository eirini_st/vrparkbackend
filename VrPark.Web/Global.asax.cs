using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace VrPark.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected static Logger logger = LogManager.GetLogger("Controller");
        private static Boolean logStackTrace = Convert.ToBoolean(ConfigurationManager.AppSettings["LogStackTrace"]);

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            logger.Error(ex.Message);
            if (logStackTrace)
            {
                logger.Error(ex.StackTrace);
            }
            if (ex.InnerException != null)
            {
                logger.Error(ex.InnerException);
                if (logStackTrace)
                {
                    logger.Error(ex.InnerException.StackTrace);
                }
            }
        }
    }
}
