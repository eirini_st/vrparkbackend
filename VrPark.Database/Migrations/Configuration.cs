namespace VrPark.Database.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using VrPark.Database.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            InitializeIdentityForEF(context);
            base.Seed(context);
        }

        public static void InitializeIdentityForEF(ApplicationDbContext db)
        {
            if (!db.Users.Any())
            {
                var roleStore = new RoleStore<IdentityRole>(db);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(db);
                var userManager = new UserManager<ApplicationUser>(userStore);

                // Add missing roles
                var role = roleManager.FindByName("Admin");
                if (role == null)
                {
                    role = new IdentityRole("Admin");
                    roleManager.Create(role);
                }

                role = roleManager.FindByName("User");
                if (role == null)
                {
                    role = new IdentityRole("User");
                    roleManager.Create(role);
                }

                role = roleManager.FindByName("TestRole");
                if (role == null)
                {
                    role = new IdentityRole("TestRole");
                    roleManager.Create(role);
                }

                // Create test users
                var adminUser = userManager.FindByName("admin");
                if (adminUser == null)
                {
                    var newAdminUser = new ApplicationUser()
                    {
                        Email = "estergian@gmail.com",
                        EmailConfirmed = true,
                        UserName = "admin",
                        FirstName = "Admin",
                        LastName = "User",
                    };
                    userManager.Create(newAdminUser, "123456");
                    userManager.SetLockoutEnabled(newAdminUser.Id, false);
                    userManager.AddToRole(newAdminUser.Id, "Admin");
                }
            }
        }
    }
}
