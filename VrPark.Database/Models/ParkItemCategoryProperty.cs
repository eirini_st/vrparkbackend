﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VrPark.Database.Models
{
    public class ParkItemCategoryProperty
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int ParkItemCategoryId { get; set; }

        [ForeignKey("ParkItemCategoryId")]
        public ParkItemCategory ParkItemCategory { get; set; }

        public int ParkItemPropertyId { get; set; }

        [ForeignKey("ParkItemPropertyId")]
        public ParkItemCategory ParkItemProperty { get; set; }

        public String Value { get; set; }
    }
}
