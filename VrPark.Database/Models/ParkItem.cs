﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VrPark.Database.Models
{
    public class ParkItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int ParkItemCategoryId { get; set; }

        [ForeignKey("ParkItemCategoryId")]
        public ParkItemCategory ParkItemCategory { get; set; }

        public String Code { get; set; }

        public String Name { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public String Description { get; set; }

        public String AdditionalInformation { get; set; }

        public virtual ICollection<ParkItemMedia> ParkItemMedia { get; set; }
    }
}
