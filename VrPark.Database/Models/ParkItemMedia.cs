﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VrPark.Database.Models
{
    public class ParkItemMedia
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int ParkItemId { get; set; }

        [ForeignKey("ParkItemId")]
        public ParkItem ParkItem { get; set; }

        public String Name { get; set; }

        public String Type { get; set; }

        public byte[] MediaContent { get; set; }
    }
}
