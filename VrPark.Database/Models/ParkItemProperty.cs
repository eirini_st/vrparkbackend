﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VrPark.Database.Models
{
    public class ParkItemProperty
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public String DsiplayedName { get; set; }

        public String Name { get; set; }

        public String Pattern { get; set; }

        public String Format { get; set; }
    }
}
