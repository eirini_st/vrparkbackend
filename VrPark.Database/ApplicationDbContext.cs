﻿using System.Data.Entity;
using VrPark.Database.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VrPark.Database
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("VrParkConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            System.Data.Entity.Database.SetInitializer<ApplicationDbContext>(null);

            modelBuilder.Entity<ParkItem>().Property(pit => pit.Latitude).HasPrecision(12, 9);
            modelBuilder.Entity<ParkItem>().Property(pit => pit.Longitude).HasPrecision(12, 9);

            modelBuilder.Entity<ParkItemCategoryProperty>()
                .HasRequired(c => c.ParkItemCategory)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ParkItemCategoryProperty>()
                .HasRequired(s => s.ParkItemProperty)
                .WithMany()
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<ParkItem> ParkItems { get; set; }

        public DbSet<ParkItemCategory> ParkItemCategories { get; set; }

        public DbSet<ParkItemProperty> ParkItemProperties { get; set; }

        public DbSet<ParkItemCategoryProperty> ParkItemCategoryProperties { get; set; }
    }
}